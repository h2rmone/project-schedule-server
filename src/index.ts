import { ApolloServer } from 'apollo-server'
import { importSchema } from 'graphql-import' 
import resolvers from './resolvers'
import bootstrapConnection from './db'

const typeDefs = importSchema('src/schemes/scheme.graphql')

async function bootstrap () {
  const db = await bootstrapConnection()

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req }) => {
      return { ...req, db }
    }
  })
  
  server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`)
  })
}

bootstrap()