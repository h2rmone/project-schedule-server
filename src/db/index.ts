import 'reflect-metadata'
import { ConnectionOptions, createConnection } from 'typeorm'

import { Project } from '../entities/project'
import { ProjectUnit } from '../entities/project-unit'

const option: ConnectionOptions = {
  name: 'mysql',
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'admin',
  database: 'test',
  synchronize: true,
  entities: [Project, ProjectUnit],
  logging: false,
}

async function bootstrapConnection () {
  return createConnection(option)
}

export default bootstrapConnection
