import project from './project'

const resolvers = {
  Query: {
    ...project.Query,
  },

  Mutation: {
    ...project.Mutation,
  },
}

export default resolvers