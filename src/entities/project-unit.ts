import { Column, Entity, ManyToOne } from 'typeorm'
import { UUIDBaseEntity } from './base'
import { Project } from './project'

enum ProjectUnitStatus {
  DEV = 'DEV',
  TEST = 'TEST',
  PRE = 'PRE',
  PUBLISH = 'PUBLISH',
}

@Entity()
export class ProjectUnit extends UUIDBaseEntity {
  @Column({
    type: "enum",
    enum: ProjectUnitStatus
  })
  status: ProjectUnitStatus;

  @Column({
    type: "timestamp",
    default: () => "CURRENT_TIMESTAMP"
  })
  startTime: string;

  @Column({
    type: "timestamp",
    default: () => "CURRENT_TIMESTAMP"
  })
  endTime: string;

  @Column()
  owner: string;

  @ManyToOne(
    () => Project,
    project => project.units,
  )
  project: Project;
}
