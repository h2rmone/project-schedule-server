import {
  Entity,
  PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn,
} from "typeorm"

@Entity()
export abstract class BaseEntity {
  @CreateDateColumn({
    type: 'timestamp',
  })
  createdAt: string

  @UpdateDateColumn({
    type: 'timestamp',
  })
  updatedAt: string

}

@Entity()
export class UUIDBaseEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string
}

