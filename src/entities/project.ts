import { Column, Entity, OneToMany } from 'typeorm'
import { UUIDBaseEntity } from './base'
import { ProjectUnit } from './project-unit'

enum ProjectStatus {
  NOT_STARTED = 'NOT_STARTED',
  DEV = 'DEV',
  TEST = 'TEST',
  PRE = 'PRE',
  PUBLISH = 'PUBLISH',
  FINISHED = 'FINISHED',
}

@Entity()
export class Project extends UUIDBaseEntity {
  @Column({
    nullable: false,
    unique: true,
  })
  name: string

  @Column({
    type: 'enum',
    enum: ProjectStatus,
    default: ProjectStatus.NOT_STARTED,
  })
  status: ProjectStatus

  @OneToMany(
    () => ProjectUnit,
    projectUnit => projectUnit.project,
  )
  units: ProjectUnit[]
}


